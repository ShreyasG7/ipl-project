# IPL Project 1
Tasks:
*  Number of matches played per year for all the years in IPL.
*  Number of matches won of per team per year in IPL.
*  Extra runs conceded per team in 2016
*  Top 10 economical bowlers in 2015

# IPL Project 2
Tasks:
*  Use a static server to serve output files.
*  Make a http request to fetch the json file
*  Display the data as a visualisation 
*  Use highcharts library for the visualisation
