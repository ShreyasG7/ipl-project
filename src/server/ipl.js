const matchesPlayed = matches => {
  //Returns the number of matches played
  ///in each year in IPL
  return matches.reduce((totalMatchesPlayed, match) => {
    totalMatchesPlayed[match.season] =
      (totalMatchesPlayed[match.season] || 0) + 1;
    return totalMatchesPlayed;
  }, {});
};

const matchesWon = matches => {
  //Returns the number of matches won
  //per team per year in IPL
  return matches.reduce((matchesWonTeamYear, match) => {
    if (matchesWonTeamYear[match.season] === undefined) {
      matchesWonTeamYear[match.season] = {};
    }
    matchesWonTeamYear[match.season][match.winner] =
      (matchesWonTeamYear[match.season][match.winner] || 0) + 1;
    return matchesWonTeamYear;
  }, {});
};

const extraRuns = (deliveries, matches, year) => {
  //Extra runs conceded by each team
  //in the year 2016 in IPL
  const matchIDsYear = matches
    .filter(match => match.season === year.toString())
    .reduce((matchesYear, match) => {
      matchesYear[match.id] = 1;
      return matchesYear;
    }, {});
  return deliveries
    .filter(delivery => delivery.match_id in matchIDsYear)
    .reduce((extraRunsTeam, delivery) => {
      extraRunsTeam[delivery.bowling_team] =
        (extraRunsTeam[delivery.bowling_team] || 0) +
        parseInt(delivery.extra_runs);
      return extraRunsTeam;
    }, {});
};

const ecoBowlers = (deliveries, matches, year) => {
  // Returns the 10 most economical bowlers
  // in the year 2015 in IPL
  const matchIDsYear = matches
    .filter(match => match.season === year.toString())
    .reduce((matchesYear, match) => {
      matchesYear[match.id] = 1;
      return matchesYear;
    }, {});
  const bowlerRunsOverYear = deliveries
    .filter(delivery => delivery.match_id in matchIDsYear)
    .reduce((bowlerRunsOvers, delivery) => {
      if (bowlerRunsOvers[delivery.bowler] === undefined) {
        bowlerRunsOvers[delivery.bowler] = {};
      }
      bowlerRunsOvers[delivery.bowler].runs =
        (bowlerRunsOvers[delivery.bowler].runs || 0) +
        parseInt(delivery.total_runs) -
        parseInt(delivery.bye_runs) -
        parseInt(delivery.legbye_runs);
      if (!parseInt(delivery.wide_runs) && !parseInt(delivery.noball_runs)) {
        bowlerRunsOvers[delivery.bowler].overs =
          (bowlerRunsOvers[delivery.bowler].overs || 0) + 1 / 6;
      }
      return bowlerRunsOvers;
    }, {});
  const bowlerEcoYear = Object.entries(bowlerRunsOverYear).reduce(
    (bowlerEco, bowler) => {
      if (bowler[1].overs >= 2) {
        bowlerEco[bowler[0]] = bowler[1].runs / bowler[1].overs;
      }
      return bowlerEco;
    },
    {}
  );
  return Object.entries(bowlerEcoYear)
    .sort((a, b) => a[1] - b[1])
    .slice(0, 10)
    .reduce((topEcoBowler, bowler) => {
      topEcoBowler[bowler[0]] = parseFloat(bowler[1].toFixed(2));
      return topEcoBowler;
    }, {});
};

module.exports = {
  matchesPlayed,
  matchesWon,
  extraRuns,
  ecoBowlers
};
