//Third party functions
const csvToJSON = require("csvtojson");
const fs = require("fs");

//Local imports
const { matchesPlayed, matchesWon, extraRuns, ecoBowlers } = require("./ipl");

//File paths to the data to be worked upon
const deliveriesPath = "../data/deliveries.csv";
const matchesPath = "../data/matches.csv";

//General function to invoke each function module
//and dump output in respective json files
const outputToFile = (
  funcName,
  outputPath,
  matchesPath,
  deliveriesPath = "",
  year = 0
) => {
  if (deliveriesPath === "") {
    csvToJSON()
      .fromFile(matchesPath)
      .then(matches => {
        const output = JSON.stringify(funcName(matches), null, 2);
        fs.writeFile(outputPath, output, "utf8", err => {
          if (err) throw err;
          console.log("Saved");
        });
      });
  } else {
    csvToJSON()
      .fromFile(deliveriesPath)
      .then(deliveries => {
        csvToJSON()
          .fromFile(matchesPath)
          .then(matches => {
            const output = JSON.stringify(
              funcName(deliveries, matches, year),
              null,
              2
            );
            fs.writeFile(outputPath, output, "utf8", err => {
              if (err) throw err;
              console.log("Saved");
            });
          });
      });
  }
};

//Function calls
outputToFile(matchesPlayed, "../output/matchesPlayed.json", matchesPath);
outputToFile(matchesWon, "../output/matchesWon.json", matchesPath);
outputToFile(
  extraRuns,
  "../output/extraRuns.json",
  matchesPath,
  deliveriesPath,
  (year = 2016)
);
outputToFile(
  ecoBowlers,
  "../output/ecoBowlers.json",
  matchesPath,
  deliveriesPath,
  (year = 2015)
);
