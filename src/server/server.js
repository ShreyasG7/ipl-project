const http = require('http');
const fs = require('fs');
const path = require('path');
const port = 3000;

const server = http.createServer((req, res) => {
  switch (req.url) {
    case '/':
      let htmlPath = path.join(__dirname, '../client/index.html');
      fs.readFile(htmlPath, 'UTF-8', (error, data) => {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end(data);
      });
      break;

    case '/app.js':
      let chartsPath = path.join(__dirname, '../client', req.url);
      let chartsStream = fs.createReadStream(chartsPath, 'UTF-8');
      res.writeHead(200, { 'Content-Type': 'text/javascript' });
      chartsStream.pipe(res);
      break;

    case '/matchesPlayed.json':
      let matchesPlayedPath = path.join(__dirname, '../output', req.url);
      let matchesPlayedStream = fs.createReadStream(matchesPlayedPath, 'UTF-8');
      res.writeHead(200, { 'Content-Type': 'text/json' });
      matchesPlayedStream.pipe(res);
      break;

    case '/matchesWon.json':
      let matchesWonPath = path.join(__dirname, '../output', req.url);
      let matchesWonStream = fs.createReadStream(matchesWonPath, 'UTF-8');
      res.writeHead(200, { 'Content-Type': 'text/json' });
      matchesWonStream.pipe(res);
      break;

    case '/extraRuns.json':
      let extraRunsPath = path.join(__dirname, '../output', req.url);
      let extraRunsStream = fs.createReadStream(extraRunsPath, 'UTF-8');
      res.writeHead(200, { 'Content-Type': 'text/json' });
      extraRunsStream.pipe(res);
      break;

    case '/ecoBowlers.json':
      let ecoBowlersPath = path.join(__dirname, '../output', req.url);
      let ecoBowlersStream = fs.createReadStream(ecoBowlersPath, 'UTF-8');
      res.writeHead(200, { 'Content-Type': 'text/json' });
      ecoBowlersStream.pipe(res);
      break;

    case '/app.css':
      let cssPath = path.join(__dirname, '../client', req.url);
      let cssStream = fs.createReadStream(cssPath, 'UTF-8');
      res.writeHead(200, { 'Content-Type': 'text/css' });
      cssStream.pipe(res);
      break;
  }
});

server.listen(port, error => {
  if (error) {
    console.log(`Something went wrong ${error}`);
  } else {
    console.log(`Listening on port ${port}`);
  }
});
