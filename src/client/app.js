const chars = {
  chart: {
    // type: 'column'
  },
  credits: {
    enabled: false
  },
  title: {
    text: ''
  },
  tooltip: {
    borderColor: '#000000',
    borderRadius: 20
  },
  yAxis: {
    title: {
      text: ''
    }
  },
  xAxis: {
    name: '',
    categories: []
  },
  series: [
    {
      name: '',
      data: []
    }
  ]
};
fetch('matchesPlayed.json')
  .then(res => res.json())
  .then(matchesPlayed => {
    chars.series[0].data = [...Object.values(matchesPlayed)];
    chars.series[0].name = 'Matches played';
    chars.xAxis.categories = [...Object.keys(matchesPlayed)];
    chars.xAxis.categories = chars.xAxis.categories.map(
      category => `<strong>${category}</strong>`
    );
    chars.xAxis.name = 'IPL seasons';
    chars.yAxis.title.text = 'Matches played per year';
    chars.title.text = '<strong>Matches played per year</strong>';
    Highcharts.chart('matchesPlayed', chars);
  });

fetch('extraRuns.json')
  .then(res => res.json())
  .then(extraRuns => {
    chars.series[0].data = [...Object.values(extraRuns)];
    chars.series[0].name = 'Extra Runs';
    chars.xAxis.categories = [...Object.keys(extraRuns)];
    chars.xAxis.categories = chars.xAxis.categories.map(
      category => `<strong>${category}</strong>`
    );
    chars.yAxis.title.text = 'Extra runs conceded';
    chars.title.text = '<strong>Extra runs conceded in 2016</strong>';
    Highcharts.chart('extraRuns', chars);
  });

fetch('ecoBowlers.json')
  .then(res => res.json())
  .then(ecoBowlers => {
    chars.series[0].data = [...Object.values(ecoBowlers)];
    chars.series[0].name = 'Economy';
    chars.xAxis.categories = [...Object.keys(ecoBowlers)];
    chars.xAxis.categories = chars.xAxis.categories.map(
      category => `<strong>${category}</strong>`
    );
    chars.xAxis.name = 'IPL seasons';
    chars.yAxis.title.text = 'Economy';
    chars.title.text = '<strong>Top economical bowlers in 2015</strong>';
    Highcharts.chart('topEcoBowlers', chars);
  });

fetch('matchesWon.json')
  .then(res => res.json())
  .then(matchesWonPerYear => {
    chars.chart.type = 'column';
    chars.chart.zoomType = 'xy';
    chars.xAxis.categories = [...Object.keys(matchesWonPerYear)];

    const allYearIPL = Object.keys(matchesWonPerYear).reduce(
      (allYearsPlayed, eachYear) => {
        allYearsPlayed[eachYear] = 0;
        return allYearsPlayed;
      },
      {}
    );

    const uniqueTeamsAllYearsIPL = Object.entries(matchesWonPerYear).reduce(
      (uniqueTeamNames, teamDetailsEachYear) => {
        Object.keys(teamDetailsEachYear[1]).reduce(
          (uniqueTeamNamesYear, team) => {
            if (team === 'Pune Warriors' || team === 'Rising Pune Supergiant') {
              uniqueTeamNames['Rising Pune Supergiants'] = 1;
            } else if (team === 'Deccan Chargers') {
              uniqueTeamNames['Sunrisers Hyderabad'] = 1;
            } else if (team !== '') {
              uniqueTeamNames[team] = 1;
            }
            return uniqueTeamNames;
          },
          uniqueTeamNames
        );
        return uniqueTeamNames;
      },
      {}
    );

    const winsOfAllTeamsForAllYears = Object.keys(
      uniqueTeamsAllYearsIPL
    ).reduce((winsOfAllTeamsForAllYears, team) => {
      let allWinsOfOneTeam = Object.entries(matchesWonPerYear).reduce(
        (eachTeamsWinsForAllYears, seasonDetails) => {
          if (team === 'Sunrisers Hyderabad') {
            eachTeamsWinsForAllYears.name = team;
            eachTeamsWinsForAllYears.data = eachTeamsWinsForAllYears.data || [];
            let winsForThatSeason =
              (seasonDetails[1]['Sunrisers Hyderabad'] || 0) +
              (seasonDetails[1]['Deccan Chargers'] || 0);
            eachTeamsWinsForAllYears.data.push(winsForThatSeason);
          } else if (team === 'Rising Pune Supergiants') {
            eachTeamsWinsForAllYears.name = team;
            eachTeamsWinsForAllYears.data = eachTeamsWinsForAllYears.data || [];
            let winsForThatSeason =
              (seasonDetails[1]['Pune Warriors'] || 0) +
              (seasonDetails[1]['Rising Pune Supergiant'] || 0) +
              (seasonDetails[1]['Rising Pune Supergiants'] || 0);
            eachTeamsWinsForAllYears.data.push(winsForThatSeason);
          } else {
            eachTeamsWinsForAllYears.name = team;
            eachTeamsWinsForAllYears.data = eachTeamsWinsForAllYears.data || [];
            eachTeamsWinsForAllYears.data.push(
              seasonDetails[1][team] || allYearIPL[seasonDetails[0]]
            );
          }
          return eachTeamsWinsForAllYears;
        },
        {}
      );
      winsOfAllTeamsForAllYears.push(allWinsOfOneTeam);
      return winsOfAllTeamsForAllYears;
    }, []);
    chars.series = winsOfAllTeamsForAllYears;

    chars.xAxis.name = 'IPL seasons';
    chars.yAxis.title.text = 'Matches won per year';
    chars.title.text = '<strong>Matches won per year by each team</strong>';
    Highcharts.chart('matchesWonPerYear', chars);
  });
